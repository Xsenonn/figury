package figury;

        import java.awt.*;
        import java.awt.geom.AffineTransform;
        import java.awt.geom.Area;
        import java.awt.geom.Ellipse2D;

class Elipsa extends Figura
{

    public Elipsa(Graphics2D buf, int del, int w, int h) {
        super(buf, del, w, h);
        if(w > 15 && h > 10)
        {
            w = 15;
            h = 10;
            shape = new Ellipse2D.Float(0,0,w,h);
        }
        else
        {
            shape = new Ellipse2D.Float(0,0,w,h);
        }
        aft = new AffineTransform();
        area = new Area(shape);
    }

}
