package figury;

import java.awt.*;

import javax.swing.*;

import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ActionEvent;

public class AnimatorApp extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try {
					final AnimatorApp frame = new AnimatorApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param delay 
	 */
	public AnimatorApp()
	{
		this.setDefaultCloseOperation(3);

		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int ww = 800;
		int wh = 900;
		setBounds((screen.width-ww)/2, (screen.height-wh)/2, ww, wh);
		
		contentPane = new JPanel();
		contentPane.setLayout(new GridLayout(2,1,0,0));
		this.setContentPane(contentPane);
		this.setBackground(Color.WHITE);


		//======== Drawing Stuff ====================
		JPanel graphicsPanel = new JPanel(new GridLayout(1,1,50,50));

		AnimPanel kanwa = new AnimPanel();
		kanwa.setLayout(new FlowLayout(FlowLayout.CENTER));
		kanwa.setBounds(0, 0, graphicsPanel.getWidth(), graphicsPanel.getHeight()-100);

		graphicsPanel.addComponentListener(new ComponentListener()
		{

			@Override
			public void componentHidden(ComponentEvent arg0) { /* Not Used */}

			@Override
			public void componentMoved(ComponentEvent arg0) { /* Not Used */}

			@Override
			public void componentResized(ComponentEvent e)
			{
				//Dimension d = e.getComponent().getSize();
				Rectangle gp = graphicsPanel.getBounds();
				kanwa.resizer(gp.width,gp.height);
				//kanwa.setBounds(0,0,d.height,d.width);
				kanwa.setBounds(0,0,gp.width,gp.height);

			}

			@Override
			public void componentShown(ComponentEvent arg0) {/* Not Used */}
	    });


		graphicsPanel.add(kanwa,BorderLayout.CENTER);
		contentPane.add(graphicsPanel);
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				kanwa.initialize();
			}
		});


		// ====== Buttons =======================
		JPanel buttPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.addFig();
			}
		});
		btnAdd.setBounds(10, 239, 80, 23);


		JButton btnAnimate = new JButton("Animate");
		btnAnimate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.animate();
				Figura.jumpFrames();
			}
		});
		btnAnimate.setBounds(10, 10, 80, 23);
		//buttPanel.add(new JLabel(""));
		//buttPanel.add(new JLabel(""));
		buttPanel.add(btnAnimate);
		buttPanel.add(btnAdd);
		//buttPanel.add(new JLabel(""));
		//buttPanel.add(new JLabel(""));
		contentPane.add(buttPanel);




		
	}

}
