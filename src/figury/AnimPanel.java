package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Area;
import java.lang.reflect.Array;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener {
	/**
	 * 
	 */


	public static ArrayList<Thread> tList = new ArrayList<Thread>();
	public static ArrayList<Figura> fList = new ArrayList<Figura>();


	private static final long serialVersionUID = 1L;

	public static int i = Thread.activeCount();
	// bufor
	public Image image;
	// wykreslacz ekranowy
	public  Graphics2D device;
	// wykreslacz bufora
	public Graphics2D buffer;



	private int delay = 70;

	private Timer timer;

	private static int numer = 0;

	public AnimPanel() {
		super();
		setBackground(Color.WHITE);
		timer = new Timer(delay, this);
	}

	public void initialize() {
		//Pobieram dlugosc i wysokosc panelu
		int width = this.getWidth();
		int height = this.getHeight();

		//Za pomocą powyższych wartości ustalam bufor
		this.image = createImage(width, height);

		//Wypełniam wykreślacz bufora
		this.buffer = (Graphics2D) this.image.getGraphics();
		this.buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		//Wypełniam wykreślacz ekranowy
		this.device = (Graphics2D) getGraphics();
		this.device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	}
	public void resizer(int width, int height)
	{
		//Zerowanie
		this.image = null;
		this.buffer = null;
		this.device = null;
		//Nadawanie ponownych wartości ze zmienionymi parametrami
		this.image = createImage(width,height);
		this.buffer = (Graphics2D) this.image.getGraphics();
		this.buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		this.device = (Graphics2D) getGraphics();
		this.device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		//Jest ich trochę więcej niż na ekranie....
		for(Figura fig : fList)
		{
			fig.buffer = buffer;
			fig.area = new Area(fig.area);

		}

		i = Thread.activeCount();
		System.out.println(i);



	}

	void addFig() {
		Figura fig = (numer++ % 2 == 0) ? new Kwadrat(buffer, delay, getWidth(), getHeight())
				: new Elipsa(buffer, delay, getWidth(), getHeight());
		fList.add(fig);
		timer.addActionListener(fig);
		Thread t = new Thread(fig);
		tList.add(t);
		t.start();
	}

	void animate() {
		if (timer.isRunning()) {
			timer.stop();
		} else {
			timer.start();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		device.drawImage(image, 0, 0, null);
		buffer.clearRect(0, 0, getWidth(), getHeight());
	}
}



