package figury;


import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;

class Kwadrat extends Figura
{

    public Kwadrat(Graphics2D buf, int del, int w, int h) {
        super(buf, del, w, h);
        shape = new Rectangle2D.Float(0,0,w,h);

        if(w > 10 && h > 10)
        {
            w = 10;
            h = 10;
            shape = new Rectangle2D.Float(0,0,w,h);
        }
        else
        {
            shape = new Rectangle2D.Float(0,0,w,h);
        }
        aft = new AffineTransform();
        area = new Area(shape);
    }

}

